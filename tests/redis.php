<?php

require "predis/autoload.php";
Predis\Autoloader::register();


$redis = new Predis\Client(array(
    "scheme" => "tcp",
    "host" => "localhost"
));

echo "Connected to Redis";
