<?php
namespace Limonada\Silex;

use Silex\Application;
use Silex\ServiceProviderInterface;

class MessagingServiceProvider implements ServiceProviderInterface {
    public function register(Application $app) {
        $app['messaging'] = $app->share( function() use( $app ) {
            return new \Limonada\Messaging($app['predis']);
        });
    }

    public function boot(Application $app) {}
}
