<?php
namespace Limonada\Silex;

use Silex\Application;
use Silex\ServiceProviderInterface;

class GCMServiceProvider implements ServiceProviderInterface {
    public function register(Application $app) {
        $app['gcm'] = $app->share( function() use( $app ) {
            return new \Limonada\GCM($app['predis'],GCM_KEY);
        });
    }

    public function boot(Application $app) {}
}
