<?php

namespace Limonada;

use Silex\Application;
use Silex\ExtensionInterface;

class GCM
{
    private $predis;
    private $gcm_key;
    private $url = 'https://gcm-http.googleapis.com/gcm/send';

    function __construct($redis,$gcm_key)
    {
        $this->predis = $redis;
        $this->gcm_key = $gcm_key;
    }

    function pokeUser($token)
    {
        $post = array('to' => $token);

        $headers = array(
            'Authorization: key='.$this->gcm_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $this->url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );
        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            throw new Exception(curl_error($ch));;
        }
        curl_close( $ch );
        echo $result;
    }
}
