<?php

namespace Limonada;

use Silex\Application;
use Silex\ExtensionInterface;

class Messaging
{
    private $predis;

    function __construct($redis)
    {
        $this->predis = $redis;
    }

    function insertMessages($messages)
    {
        foreach ($messages as $m)
        {
            $this->predis->lpush(
                $this->getUserQueueVarname($m['recipient']),
                $this->encodeMessage($m,$m['recipient'])
            );
        }
    }

    function getUserNextMessageIdVarname($userId)
    {
        return sprintf('last_message_id:queue:user:%s',$userId);
    }

    function getUserQueueVarname($userId)
    {
        return sprintf('message_queue:user:%s',$userId);
    }

    function getUserInfoVarname($userId) {
        return sprintf('info:user:%s',$userId);
    }

    function encodeMessage(&$message,$userId)
    {
        $message_id = $this->predis->incr($this->getUserNextMessageIdVarname($userId));
        $message['message_id'] = $message_id;
        $message['server_timestamp'] = time();
        return json_encode($message);
    }

    function queryMessagesForUser($userId)
    {
        return $this->predis->lrange($this->getUserQueueVarname($userId),0,-1);
    }

    function popMessagesForUser($userId,$limit)
    {
        $queueToPop = $this->getUserQueueVarname($userId);
        $messages = array();
        $i = 0;

        while ($i < $limit and $message = $this->predis->rpop($queueToPop))
        {
            $messages[] = json_decode($message);
            $i++;
        }

        return $messages;
    }

    function registerUser($user,$token) {
        if (!$this->isValidToken($token)) {
           throw new Exception('Unable to validate token');
        }

        $userInfo = $this->getUserInfo($user);
        $userInfo['user'] = $user;
        $userInfo['token'] = $token;
        $this->setUserInfo($user,$userInfo);
    }

    function setUserInfo($user,$userInfo) {
        $varname = $this->getUserInfoVarname($user);
        $this->predis->set($varname,json_encode($userInfo));
    }

    function getUserInfo($user) {
        $varname = $this->getUserInfoVarname($user);
        $userInfo = $this->predis->get($varname);
        if (!$userInfo) {$userInfo = array();}
        else {$userInfo = json_decode($userInfo,true);}

        return $userInfo;
    }

    function isValidToken($token) {
        //@todo ask google if so

        return true;
    }
}
