<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../.gcm_key.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

$app = new Silex\Application();
$app['debug'] = true;

// decode json object before passing it to the handler
$app->before(function (Request $request)
{
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json'))
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }

    // @todo token validation
});

// register libraries
$app->register(new Predis\Silex\ClientServiceProvider(), array(
    'predis.parameters' => 'tcp://127.0.0.1:6379',
    'predis.options'    => [
        'profile' => '3.0',
    ],
));
$app->register(new Limonada\Silex\MessagingServiceProvider(), array());
$app->register(new Limonada\Silex\GCMServiceProvider(), array('gcm_key' => GCM_KEY));



// get new user messages
$app->post('/pop_messages', function (Request $request) use ($app)
{
    $recipient = $request->request->get('recipient');
    $messages = $app['messaging']->popMessagesForUser($recipient,50);
    $response = array('messages' => $messages);

    return $app->json($response);
});

// post messages
$app->post('/post_message', function (Request $request) use ($app)
{
    $recipient = $request->request->get('recipient');
    $message = array(
        'sender' => $request->request->get('sender'),
        'recipient' => $recipient,
        'text' => $request->request->get('text'),
        'client_timestamp' => $request->request->get('client_timestamp'),
    );

    $app['messaging']->insertMessages(array($message));
    $userInfo = $app['messaging']->getUserInfo($recipient);
    $app['gcm']->pokeUser($userInfo['token']);

    return $app->json(array('message' => 'Great success!'));
});

// post messages
$app->post('/register_user', function (Request $request) use ($app)
{
    $user = $request->request->get('username');
    $token = $request->request->get('token');

    $app['messaging']->registerUser($user,$token);

    return $app->json(array('message' => "Created the user $user, now broadcasting like a champ"));
});

$app->run();

